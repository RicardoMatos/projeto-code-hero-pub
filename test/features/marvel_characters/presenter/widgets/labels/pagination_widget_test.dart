import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';

import 'package:code_hero_project/core/di/di.dart';
import 'package:code_hero_project/features/marvel_characters/presenter/widgets/pagination/pagination_button.dart';
import 'package:code_hero_project/features/marvel_characters/presenter/widgets/pagination/pagination_widget.dart';

void main() {
  setUp(() async {
    await GetIt.I.reset();
  });
  testWidgets(
      'Should create PaginationButton = pageQtd when pageQtd < maxButtons',
      (widgetTester) async {
    DI.init();

    const pageQtd = 5;
    const maxButtons = 10;

    await widgetTester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: Column(
      children: const [
        PaginationWidget(
            selectedPage: 1, pageQtd: pageQtd, maxButtons: maxButtons),
      ],
    ))));

    final finder = find.byType(PaginationButton, skipOffstage: false);
    final count = widgetTester.widgetList<PaginationButton>(finder).length;

    expect(count, pageQtd);
  });

  testWidgets(
      'Should create PaginationButton = maxButtons when pageQtd > maxButtons',
      (widgetTester) async {
    DI.init();

    const pageQtd = 16;
    const maxButtons = 10;

    await widgetTester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: Column(
      children: const [
        PaginationWidget(
            selectedPage: 1, pageQtd: pageQtd, maxButtons: maxButtons),
      ],
    ))));

    final finder = find.byType(PaginationButton, skipOffstage: false);
    final count = widgetTester.widgetList<PaginationButton>(finder).length;

    expect(count, maxButtons);
  });
}
