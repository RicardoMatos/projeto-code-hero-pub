import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';

import 'package:code_hero_project/core/di/di.dart';
import 'package:code_hero_project/features/marvel_characters/presenter/widgets/labels/candidate_name_widget.dart';

void main() {
  setUpAll(() async {
    await GetIt.I.reset();
  });
  testWidgets('Should show data correctly', (widgetTester) async {
    DI.init();

    await widgetTester.pumpWidget(const Directionality(
        textDirection: TextDirection.ltr,
        child: CandidateName(name: 'Ricardo Matos')));

    final text = find.text('Ricardo Matos'.toUpperCase());

    expect(text, findsOneWidget);
  });
}
