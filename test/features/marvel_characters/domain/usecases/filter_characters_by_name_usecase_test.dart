import 'dart:convert';

import 'package:code_hero_project/core/data/repositories/model/reponse_model.dart';
import 'package:code_hero_project/core/utils/extensions/either_extension.dart';
import 'package:code_hero_project/features/marvel_characters/data/dtos/character_dto.dart';
import 'package:code_hero_project/features/marvel_characters/domain/entities/character_entity.dart';
import 'package:code_hero_project/features/marvel_characters/domain/repositories/filter_characters_by_name_repository.dart';
import 'package:code_hero_project/features/marvel_characters/domain/usecases/filter_characters_by_name_usecase/filter_characters_by_name_usecase.dart';
import 'package:code_hero_project/features/marvel_characters/domain/usecases/filter_characters_by_name_usecase/filter_characters_by_name_usecase_imp.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../utils.dart';
import 'filter_characters_by_name_usecase_test.mocks.dart';

class FilterCharactersByNameRepositoryTest extends Mock
    implements FilterCharactersByNameRepository {}

@GenerateMocks([FilterCharactersByNameRepositoryTest])
void main() {
  late FilterCharactersByNameRepository repository;
  late FilterCharactersByNameUseCase usecase;

  late final ResponseDataContainer responseModel;
  late final Map<String, dynamic> apiResponse;

  setUpAll(() => () {
        repository = MockFilterCharactersByNameRepositoryTest();
        usecase = FilterCharactersByNameUseCaseImp(repository);
        apiResponse =
            json.decode(TestUtils().loadFixture('characters_by_name.json'));
        responseModel = ResponseDataWrapper.fromMap(apiResponse).container;
      });

  test(
      'Should return a ResponseDataContainer on Request',
      () => () async {
            const limit = 20;
            const offset = 0;
            const text = 'spider';

            when(repository(offset: offset, limit: limit, name: text))
                .thenAnswer((_) async => Right(responseModel));

            final result = await usecase(FilterCharactersByNameParams(
                offset: offset, limit: limit, name: text));

            expect(result, isA<ResponseDataContainer>());
            verify(repository(offset: offset, limit: limit, name: text));
          });

  test(
      'Should ResponseDataContainer > results should successfully'
      ' cast to List<CharacterEntity>',
      () => () async {
            const limit = 20;
            const offset = 0;
            const text = 'spider';

            when(repository(offset: offset, limit: limit, name: text))
                .thenAnswer((_) async => Right(responseModel));

            final result = await usecase(FilterCharactersByNameParams(
                offset: offset, limit: limit, name: text));

            final items = result.fold(
              (l) => TestFailure('Error'),
              (r) => r.results.map((e) => CharacterDTO.fromJson(e)),
            );

            expect(result, isA<ResponseDataContainer>());
            expect(items, isA<List<CharacterEntity>>());
            verify(repository(offset: offset, limit: limit, name: text));
          });

  test(
      'Should all items name start with spider',
      () => () async {
            const limit = 20;
            const offset = 0;
            const text = 'spider';

            when(repository(offset: offset, limit: limit, name: text))
                .thenAnswer((_) async => Right(responseModel));

            final result = await usecase(FilterCharactersByNameParams(
                offset: offset, limit: limit, name: text));

            var items = <CharacterEntity>[];

            var startsOkList = <CharacterEntity>[];

            if (result.isRight()) {
              items = result
                  .asRight()
                  .results
                  .map((e) => CharacterDTO.fromJson(e))
                  .toList();
              startsOkList = items
                  .where((element) => element.name.startsWith(text))
                  .toList();
            }

            expect(result, isA<ResponseDataContainer>());
            expect((items.isEmpty | startsOkList.isEmpty), false);
            expect(startsOkList.length, items.length);
            verify(repository(offset: offset, limit: limit, name: text));
          });
}
