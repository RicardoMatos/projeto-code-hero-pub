import 'dart:convert';

import 'package:code_hero_project/core/data/repositories/model/reponse_model.dart';
import 'package:code_hero_project/features/marvel_characters/data/dtos/character_dto.dart';
import 'package:code_hero_project/features/marvel_characters/domain/entities/character_entity.dart';
import 'package:code_hero_project/features/marvel_characters/domain/repositories/get_characters_repository.dart';
import 'package:code_hero_project/features/marvel_characters/domain/usecases/get_characters_usercase/get_characters_usecase.dart';
import 'package:code_hero_project/features/marvel_characters/domain/usecases/get_characters_usercase/get_characters_usecase_imp.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../../utils.dart';
import 'get_characters_usecase_imp_test.mocks.dart';

class GetCharactersRepositoryTest extends Mock
    implements GetCharactersRepository {}

@GenerateMocks([GetCharactersRepositoryTest])
void main() {
  late GetCharactersRepository repository;
  late GetCharactersUseCaseImp usecase;

  late final ResponseDataContainer responseModel;
  late final Map<String, dynamic> apiResponse;

  setUpAll(() => () {
        repository = MockGetCharactersRepositoryTest();
        usecase = GetCharactersUseCaseImp(repository);
        apiResponse = json.decode(TestUtils().loadFixture('characters.json'));
        responseModel = ResponseDataWrapper.fromMap(apiResponse).container;
      });

  test(
      'Should return a ResponseDataContainer on Request',
      () => () async {
            const limit = 20;
            const offset = 0;

            when(repository(offset: offset, limit: limit))
                .thenAnswer((_) async => Right(responseModel));

            final result = await usecase(
                GetCharactersParams(offset: offset, limit: limit));

            expect(result, isA<ResponseDataContainer>());
            verify(repository(offset: offset, limit: limit));
          });

  test(
      'Should ResponseDataContainer > results should successfully'
      ' cast to List<CharacterEntity>',
      () => () async {
            const limit = 20;
            const offset = 0;

            when(repository(offset: offset, limit: limit))
                .thenAnswer((_) async => Right(responseModel));

            final result = await usecase(
                GetCharactersParams(offset: offset, limit: limit));

            final items = result.fold(
              (l) => TestFailure('Error'),
              (r) => r.results.map((e) => CharacterDTO.fromJson(e)),
            );

            expect(result, isA<ResponseDataContainer>());
            expect(items, isA<List<CharacterEntity>>());
            verify(repository(offset: offset, limit: limit));
          });
}
