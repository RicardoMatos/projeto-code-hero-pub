import 'dart:io';

class TestUtils {
  String loadFixture(String file) {
    var dir = Directory.current.path;
    if (dir.endsWith('/test')) {
      dir = dir.replaceAll('/test', 'replace');
    }

    return File('$dir/test/fixtures/$file').readAsStringSync();
  }
}
