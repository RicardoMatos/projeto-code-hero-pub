// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class StorySummaryEntity extends Equatable {
  final String resourceUri;
  final String name;
  final String type;

  const StorySummaryEntity({
    required this.resourceUri,
    required this.name,
    required this.type,
  });

  @override
  List<Object?> get props => [resourceUri];
}
