import 'package:equatable/equatable.dart';

import 'image_entity.dart';

class CreatorEntity extends Equatable {
  final int id;
  final String firstName;
  final String lastName;
  final ImageEntity thumbnail;
  final List<ImageEntity> images;

  const CreatorEntity({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.thumbnail,
    required this.images,
  });

  @override
  List<Object?> get props => [id];
}
