// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class SerieSummaryEntity extends Equatable {
  final String resourceUri;
  final String name;

  const SerieSummaryEntity({
    required this.resourceUri,
    required this.name,
  });

  @override
  List<Object?> get props => [resourceUri];
}
