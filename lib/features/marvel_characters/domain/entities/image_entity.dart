// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class ImageEntity extends Equatable {
  const ImageEntity({
    required this.path,
    required this.extension,
  });

  final String extension;
  final String path;

  @override
  List<Object?> get props => [path, extension];

  String get standardSmall => "$path/standard_small.$extension";
  String get standardMedium => "$path/standard_medium.$extension";
  String get standardLarge => "$path/standard_large.$extension";

  String get portraitSmall => "$path/portrait_small.$extension";
  String get portraitMedium => "$path/portrait_medium.$extension";
  String get portraitXlarge => "$path/portrait_xlarge.$extension";
  String get portraitFantastic => "$path/portrait_fantastic.$extension";

  String get landscapeSmall => "$path/landscape_small.$extension";
  String get landscapeMedium => "$path/landscape_medium.$extension";
  String get landscapeLarge => "$path/landscape_large.$extension";
  String get landscapeXlarge => "$path/landscape_xlarge.$extension";
}
