// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

import 'comic_summary_entity.dart';
import 'event_summary_entity.dart';
import 'image_entity.dart';
import 'resource_list.dart';
import 'serie_summary_entity.dart';
import 'story_summary_entity.dart';
import 'url_entity.dart';

class CharacterEntity extends Equatable {
  final int id;
  final String name;
  final String description;
  final DateTime modified;
  final String resourceURI;
  final List<UrlEntity> urls;
  final ImageEntity image;
  final ResourceList<ComicSummaryEntity> comics;
  final ResourceList<EventSummaryEntity> events;
  final ResourceList<StorySummaryEntity> stories;
  final ResourceList<SerieSummaryEntity> series;

  const CharacterEntity({
    required this.id,
    required this.name,
    required this.description,
    required this.modified,
    required this.resourceURI,
    required this.urls,
    required this.image,
    required this.comics,
    required this.events,
    required this.stories,
    required this.series,
  });

  @override
  List<Object?> get props => [id];
}
