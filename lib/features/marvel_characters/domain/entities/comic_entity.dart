// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

import 'image_entity.dart';

class ComicEntity extends Equatable {
  final int id;
  final String title;
  final String description;
  final ImageEntity thumbnail;
  final List<ImageEntity> images;

  const ComicEntity({
    required this.id,
    required this.title,
    required this.description,
    required this.thumbnail,
    required this.images,
  });

  @override
  List<Object?> get props => [id];
}
