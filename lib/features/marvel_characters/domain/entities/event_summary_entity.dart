// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class EventSummaryEntity extends Equatable {
  final String resourceUri;
  final String name;
  const EventSummaryEntity({
    required this.resourceUri,
    required this.name,
  });

  @override
  List<Object?> get props => [resourceUri];
}
