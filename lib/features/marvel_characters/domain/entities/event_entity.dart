// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

import 'image_entity.dart';

class EventEntity extends Equatable {
  const EventEntity({
    required this.id,
    required this.title,
    required this.description,
    required this.thumbnail,
  });

  final String description;
  final int id;
  final ImageEntity thumbnail;
  final String title;

  @override
  List<Object?> get props => [id];
}
