// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class ResourceList<T> extends Equatable {
  const ResourceList({
    required this.available,
    required this.returned,
    required this.items,
    required this.collectionUri,
  });

  final int available;
  final String collectionUri;
  final List<T> items;
  final int returned;

  @override
  List<Object?> get props => [collectionUri];
}
