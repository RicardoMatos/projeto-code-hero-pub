import 'package:dartz/dartz.dart';

import '../../../../../core/erros/failure.dart';
import '../../../../../core/data/repositories/model/reponse_model.dart';
import '../../repositories/filter_characters_by_name_repository.dart';
import 'filter_characters_by_name_usecase.dart';

class FilterCharactersByNameUseCaseImp
    implements FilterCharactersByNameUseCase {
  final FilterCharactersByNameRepository _repository;

  FilterCharactersByNameUseCaseImp(this._repository);

  @override
  Future<Either<Failure, ResponseDataContainer>> call(
      FilterCharactersByNameParams params) async {
    return _repository(
        limit: params.limit, name: params.name, offset: params.offset);
  }
}
