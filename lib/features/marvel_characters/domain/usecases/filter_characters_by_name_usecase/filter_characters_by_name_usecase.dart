// ignore_for_file: public_member_api_docs, sort_constructors_first
import '../../../../../core/data/repositories/model/reponse_model.dart';
import '../../../../../core/domain/usecases/use_case.dart';

abstract class FilterCharactersByNameUseCase
    implements UseCase<ResponseDataContainer, FilterCharactersByNameParams> {}

class FilterCharactersByNameParams {
  final int offset;
  final int limit;
  final String name;

  FilterCharactersByNameParams({
    required this.offset,
    required this.limit,
    required this.name,
  });
}
