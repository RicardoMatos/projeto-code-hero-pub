import 'package:dartz/dartz.dart';

import '../../../../../core/data/repositories/model/reponse_model.dart';
import '../../../../../core/erros/failure.dart';
import '../../repositories/get_characters_repository.dart';
import 'get_characters_usecase.dart';

class GetCharactersUseCaseImp implements GetCharactersUseCase {
  final GetCharactersRepository repository;

  GetCharactersUseCaseImp(this.repository);

  @override
  Future<Either<Failure, ResponseDataContainer>> call(
      GetCharactersParams params) async {
    return repository(limit: params.limit, offset: params.offset);
  }
}
