// ignore_for_file: public_member_api_docs, sort_constructors_first
import '../../../../../core/data/repositories/model/reponse_model.dart';
import '../../../../../core/domain/usecases/use_case.dart';

abstract class GetCharactersUseCase
    implements UseCase<ResponseDataContainer, GetCharactersParams> {}

class GetCharactersParams {
  final int offset;
  final int limit;

  GetCharactersParams({
    required this.offset,
    required this.limit,
  });
}
