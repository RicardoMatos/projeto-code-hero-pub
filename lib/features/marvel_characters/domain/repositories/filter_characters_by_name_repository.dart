import 'package:dartz/dartz.dart';

import '../../../../core/data/repositories/model/reponse_model.dart';
import '../../../../core/erros/failure.dart';

abstract class FilterCharactersByNameRepository {
  Future<Either<Failure, ResponseDataContainer>> call(
      {required int offset, required int limit, required String name});
}
