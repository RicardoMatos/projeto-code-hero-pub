import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import '../../../../core/utils/debouncer.dart';
import '../../domain/entities/character_entity.dart';
import 'characters_controller.dart';

part 'home_page_controller.g.dart';

class HomePageController = _HomePageControllerBase with _$HomePageController;

abstract class _HomePageControllerBase with Store {
  final CharactersController _charactersController;
  _HomePageControllerBase(this._charactersController) {
    _textEditingController.addListener(() {
      _debouncer.run(
        () {
          final str = _textEditingController.text;
          if (str.isNotEmpty && str.length > 3) {
            if (searchText != str) {
              _charactersController.reset();
            }
            searchText = str;
            loadCharactersByFiltering(1);
          } else if (str.isEmpty) {
            searchText = '';
            loadCharacters(1);
          }
        },
      );
    });
  }

  static const _itemsPerList = 4;

  String searchText = '';

  @observable
  bool filtering = false;

  @observable
  int _currentPage = 1;

  @observable
  int _pagesQtd = 0;

  @observable
  bool loading = false;

  @observable
  ObservableList<CharacterEntity> _showing = ObservableList();

  final TextEditingController _textEditingController =
      TextEditingController(text: '');

  final Debouncer _debouncer = Debouncer(milliseconds: 700);

  @action
  Future<void> loadCharacters(int page) async {
    filtering = false;
    _currentPage = page;
    loading = true;
    _showing = (await _charactersController.getItems(
            qtd: _itemsPerList, offset: (page - 1) * _itemsPerList))
        .asObservable();
    _pagesQtd = _charactersController.totalItems ~/ _itemsPerList;
    loading = false;
  }

  @action
  Future<void> loadCharactersByFiltering(int page) async {
    filtering = true;
    _currentPage = page;
    loading = true;
    _showing = (await _charactersController.filterItems(
            text: searchText,
            qtd: _itemsPerList,
            offset: (page - 1) * _itemsPerList))
        .asObservable();
    _pagesQtd = _charactersController.totalItems ~/ _itemsPerList;
    loading = false;
  }

  @computed
  int get currentPage => _currentPage;

  @computed
  int get pagesQtd => _pagesQtd;

  TextEditingController get textEditingController => _textEditingController;

  ObservableList<CharacterEntity> get characters => _showing;
}
