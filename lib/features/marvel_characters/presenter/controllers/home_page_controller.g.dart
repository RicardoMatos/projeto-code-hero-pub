// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_page_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$HomePageController on _HomePageControllerBase, Store {
  Computed<int>? _$currentPageComputed;

  @override
  int get currentPage =>
      (_$currentPageComputed ??= Computed<int>(() => super.currentPage,
              name: '_HomePageControllerBase.currentPage'))
          .value;
  Computed<int>? _$pagesQtdComputed;

  @override
  int get pagesQtd =>
      (_$pagesQtdComputed ??= Computed<int>(() => super.pagesQtd,
              name: '_HomePageControllerBase.pagesQtd'))
          .value;

  late final _$filteringAtom =
      Atom(name: '_HomePageControllerBase.filtering', context: context);

  @override
  bool get filtering {
    _$filteringAtom.reportRead();
    return super.filtering;
  }

  @override
  set filtering(bool value) {
    _$filteringAtom.reportWrite(value, super.filtering, () {
      super.filtering = value;
    });
  }

  late final _$_currentPageAtom =
      Atom(name: '_HomePageControllerBase._currentPage', context: context);

  @override
  int get _currentPage {
    _$_currentPageAtom.reportRead();
    return super._currentPage;
  }

  @override
  set _currentPage(int value) {
    _$_currentPageAtom.reportWrite(value, super._currentPage, () {
      super._currentPage = value;
    });
  }

  late final _$_pagesQtdAtom =
      Atom(name: '_HomePageControllerBase._pagesQtd', context: context);

  @override
  int get _pagesQtd {
    _$_pagesQtdAtom.reportRead();
    return super._pagesQtd;
  }

  @override
  set _pagesQtd(int value) {
    _$_pagesQtdAtom.reportWrite(value, super._pagesQtd, () {
      super._pagesQtd = value;
    });
  }

  late final _$loadingAtom =
      Atom(name: '_HomePageControllerBase.loading', context: context);

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  late final _$_showingAtom =
      Atom(name: '_HomePageControllerBase._showing', context: context);

  @override
  ObservableList<CharacterEntity> get _showing {
    _$_showingAtom.reportRead();
    return super._showing;
  }

  @override
  set _showing(ObservableList<CharacterEntity> value) {
    _$_showingAtom.reportWrite(value, super._showing, () {
      super._showing = value;
    });
  }

  late final _$loadCharactersAsyncAction =
      AsyncAction('_HomePageControllerBase.loadCharacters', context: context);

  @override
  Future<void> loadCharacters(int page) {
    return _$loadCharactersAsyncAction.run(() => super.loadCharacters(page));
  }

  late final _$loadCharactersByFilteringAsyncAction = AsyncAction(
      '_HomePageControllerBase.loadCharactersByFiltering',
      context: context);

  @override
  Future<void> loadCharactersByFiltering(int page) {
    return _$loadCharactersByFilteringAsyncAction
        .run(() => super.loadCharactersByFiltering(page));
  }

  @override
  String toString() {
    return '''
filtering: ${filtering},
loading: ${loading},
currentPage: ${currentPage},
pagesQtd: ${pagesQtd}
    ''';
  }
}
