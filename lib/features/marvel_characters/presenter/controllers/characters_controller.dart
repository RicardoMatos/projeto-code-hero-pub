import 'package:mobx/mobx.dart';

import '../../data/dtos/character_dto.dart';
import '../../domain/entities/character_entity.dart';
import '../../domain/usecases/filter_characters_by_name_usecase/filter_characters_by_name_usecase.dart';
import '../../domain/usecases/get_characters_usercase/get_characters_usecase.dart';

part 'characters_controller.g.dart';

class CharactersController = _CharactersControllerBase
    with _$CharactersController;

abstract class _CharactersControllerBase with Store {
  final GetCharactersUseCase _getCharactersUseCase;
  final FilterCharactersByNameUseCase _filterByNameUseCase;

  _CharactersControllerBase(
      this._getCharactersUseCase, this._filterByNameUseCase);

  @observable
  bool fetching = false;

  @observable
  bool filtering = false;

  @observable
  // ignore: prefer_final_fields
  ObservableList<CharacterEntity> _characters = ObservableList();

  @observable
  int _totalItems = 0;

  final int _requestItemsLimit = 20;
  int _requestItemsOffset = 0;

  /// Fetch Characters paginating
  @action
  Future<void> fetchCharacters() async {
    fetching = true;
    final response = await _getCharactersUseCase(GetCharactersParams(
        offset: _requestItemsOffset, limit: _requestItemsLimit));
    response.fold(
      (l) => _characters = ObservableList(),
      (r) {
        final newCharacters = r.results
            .map((e) => CharacterDTO.fromJson(e))
            .toList()
            .asObservable();

        _characters.addAll(newCharacters);

        _requestItemsOffset += newCharacters.length;
        if (_totalItems != r.total) {
          _totalItems = r.total;
        }
      },
    );
    fetching = false;
  }

  @action
  Future<void> filterByName(String name) async {
    fetching = true;

    final response = await _filterByNameUseCase(FilterCharactersByNameParams(
        offset: _requestItemsOffset, limit: _requestItemsLimit, name: name));

    response.fold((l) => _characters = ObservableList(), (r) {
      final newCharacters = r.results
          .map((e) => CharacterDTO.fromJson(e))
          .toList()
          .asObservable();

      _characters.addAll(newCharacters);

      _requestItemsOffset += newCharacters.length;
      if (_totalItems != r.total) {
        _totalItems = r.total;
      }
    });

    fetching = false;
  }

  @action
  void reset() {
    _totalItems = 0;
    _requestItemsOffset = 0;
    _characters = ObservableList();
  }

  /// Get specific numbers of items from [characters]
  Future<List<CharacterEntity>> getItems({
    required int qtd,
    required int offset,
  }) async {
    if (filtering) {
      reset();
      filtering = false;
    }
    if (offset >= characters.length) {
      await fetchCharacters();
    }

    final end = offset + qtd;

    if (characters.isNotEmpty && characters.length > end) {
      return characters.sublist(offset, end);
    }

    if (characters.isNotEmpty && characters.length <= end) {
      return characters.sublist(offset, characters.length);
    }

    return [];
  }

  Future<List<CharacterEntity>> filterItems({
    required String text,
    required int qtd,
    required int offset,
  }) async {
    if (!filtering) {
      reset();
      filtering = true;
    }
    if (offset >= characters.length) {
      await filterByName(text);
    }

    final end = offset + qtd;

    if (characters.isNotEmpty && characters.length > end) {
      return characters.sublist(offset, end);
    }

    if (characters.isNotEmpty && characters.length <= end) {
      return characters.sublist(offset, characters.length);
    }

    return [];
  }

  ObservableList<CharacterEntity> get characters => _characters;

  @computed
  int get maxItemsQtd => _totalItems;

  @computed
  int get totalItems => _totalItems;
}
