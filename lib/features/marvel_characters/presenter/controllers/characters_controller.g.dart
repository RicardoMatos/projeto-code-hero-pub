// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'characters_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$CharactersController on _CharactersControllerBase, Store {
  Computed<int>? _$maxItemsQtdComputed;

  @override
  int get maxItemsQtd =>
      (_$maxItemsQtdComputed ??= Computed<int>(() => super.maxItemsQtd,
              name: '_CharactersControllerBase.maxItemsQtd'))
          .value;
  Computed<int>? _$totalItemsComputed;

  @override
  int get totalItems =>
      (_$totalItemsComputed ??= Computed<int>(() => super.totalItems,
              name: '_CharactersControllerBase.totalItems'))
          .value;

  late final _$fetchingAtom =
      Atom(name: '_CharactersControllerBase.fetching', context: context);

  @override
  bool get fetching {
    _$fetchingAtom.reportRead();
    return super.fetching;
  }

  @override
  set fetching(bool value) {
    _$fetchingAtom.reportWrite(value, super.fetching, () {
      super.fetching = value;
    });
  }

  late final _$filteringAtom =
      Atom(name: '_CharactersControllerBase.filtering', context: context);

  @override
  bool get filtering {
    _$filteringAtom.reportRead();
    return super.filtering;
  }

  @override
  set filtering(bool value) {
    _$filteringAtom.reportWrite(value, super.filtering, () {
      super.filtering = value;
    });
  }

  late final _$_charactersAtom =
      Atom(name: '_CharactersControllerBase._characters', context: context);

  @override
  ObservableList<CharacterEntity> get _characters {
    _$_charactersAtom.reportRead();
    return super._characters;
  }

  @override
  set _characters(ObservableList<CharacterEntity> value) {
    _$_charactersAtom.reportWrite(value, super._characters, () {
      super._characters = value;
    });
  }

  late final _$_totalItemsAtom =
      Atom(name: '_CharactersControllerBase._totalItems', context: context);

  @override
  int get _totalItems {
    _$_totalItemsAtom.reportRead();
    return super._totalItems;
  }

  @override
  set _totalItems(int value) {
    _$_totalItemsAtom.reportWrite(value, super._totalItems, () {
      super._totalItems = value;
    });
  }

  late final _$fetchCharactersAsyncAction = AsyncAction(
      '_CharactersControllerBase.fetchCharacters',
      context: context);

  @override
  Future<void> fetchCharacters() {
    return _$fetchCharactersAsyncAction.run(() => super.fetchCharacters());
  }

  late final _$filterByNameAsyncAction =
      AsyncAction('_CharactersControllerBase.filterByName', context: context);

  @override
  Future<void> filterByName(String name) {
    return _$filterByNameAsyncAction.run(() => super.filterByName(name));
  }

  late final _$_CharactersControllerBaseActionController =
      ActionController(name: '_CharactersControllerBase', context: context);

  @override
  void reset() {
    final _$actionInfo = _$_CharactersControllerBaseActionController
        .startAction(name: '_CharactersControllerBase.reset');
    try {
      return super.reset();
    } finally {
      _$_CharactersControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
fetching: ${fetching},
filtering: ${filtering},
maxItemsQtd: ${maxItemsQtd},
totalItems: ${totalItems}
    ''';
  }
}
