import 'package:flutter/material.dart';

import '../../../../../core/presenter/styles.dart';

class SummaryItem extends StatelessWidget {
  final String label;
  final String text;
  const SummaryItem({super.key, required this.label, required this.text});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        RichText(
            text: TextSpan(
                style: AppTextStyle.titleHeaderBlack
                    .copyWith(fontSize: AppFontSize.headerTitle),
                text: "$label ",
                children: [
              TextSpan(
                  text: text,
                  style: AppTextStyle.titleHeaderLight
                      .copyWith(fontSize: AppFontSize.headerTitle))
            ])),
      ],
    );
  }
}
