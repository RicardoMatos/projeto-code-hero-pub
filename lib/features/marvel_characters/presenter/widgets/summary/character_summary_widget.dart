import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../../core/di/di.dart';
import '../../../../../core/domain/service/presenter_specs_service.dart';
import '../../../../../core/presenter/styles.dart';
import '../../../domain/entities/character_entity.dart';
import 'summary_item_widget.dart';

class CharacterSummary extends StatelessWidget {
  final CharacterEntity character;
  const CharacterSummary({super.key, required this.character});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(Insets.marginHorizontalFixed),
        child: Observer(
          builder: (context) {
            final isLarge = DI.get<PresenterSpecsService>().deviceWidth >
                FormFactor.desktop;
            return Flex(
              direction: isLarge ? Axis.horizontal : Axis.vertical,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  constraints: const BoxConstraints(minWidth: 300),
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Image.network(
                        character.image.portraitXlarge,
                        height: 350,
                        fit: BoxFit.fitHeight,
                      )),
                ),
                Container(
                  constraints: BoxConstraints(
                      maxWidth: isLarge
                          ? DI.get<PresenterSpecsService>().deviceWidth * .5
                          : double.infinity),
                  margin: EdgeInsets.only(top: isLarge ? 0 : 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SummaryItem(label: 'Nome:', text: character.name),
                      SummaryItem(
                          label: 'Descrição:', text: character.description),
                      SummaryItem(
                          label: '\nEventos:\n\n',
                          text: character.events.items
                              .map((e) => e.name)
                              .toList()
                              .join(' | ')),
                      SummaryItem(
                          label: '\nSéries:\n\n',
                          text: character.series.items
                              .map((e) => e.name)
                              .join(' \n ')),
                      SummaryItem(
                          label: '\nQuadrinhos:\n\n',
                          text: character.comics.items
                              .map((e) => e.name)
                              .join(' \n ')),
                      SummaryItem(
                          label: '\nHistórias:\n\n',
                          text: character.stories.items
                              .map((e) => e.name)
                              .join(' \n ')),
                    ],
                  ),
                )
              ],
            );
          },
        ));
  }
}
