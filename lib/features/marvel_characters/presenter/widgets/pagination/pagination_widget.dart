import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../../core/presenter/styles.dart';
import 'pagination_button.dart';

class PaginationWidget extends StatelessWidget {
  final int selectedPage;
  final int pageQtd;
  final Function(int page)? onSelectPage;
  final VoidCallback? onNext;
  final VoidCallback? onPrevious;
  final int maxButtons;

  const PaginationWidget(
      {super.key,
      required this.selectedPage,
      required this.pageQtd,
      required this.maxButtons,
      this.onSelectPage,
      this.onNext,
      this.onPrevious});

  List<Widget> generateButtons() {
    final btns = <Widget>[];

    int initialPage = 0;

    int target = 0;

    if (pageQtd == 0) {
      initialPage = 1;
      target = 1;
    } else if (pageQtd <= maxButtons) {
      initialPage = 1;
      target = pageQtd;
    } else {
      final num = (maxButtons ~/ 2);
      if (selectedPage <= num) {
        initialPage = 1;
        target = maxButtons;
      } else {
        initialPage = selectedPage - num;
        target = selectedPage + num - (maxButtons.isOdd ? 0 : 1);
      }
    }

    for (int i = initialPage; i <= target; i++) {
      btns.add(PaginationButton(
        page: i,
        selected: i == selectedPage,
        applySpace: i < target,
        onTap: (page) {
          if (onSelectPage != null && pageQtd != 0) {
            onSelectPage!(page);
          }
        },
      ));
    }
    return btns;
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return Container(
        margin: EdgeInsets.only(
            top: Insets.paginantionTopMargin,
            bottom: Insets.paginantionBottomMargin),
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          IconButton(
              iconSize: Insets.paginantionNextPreviusButtonSize,
              disabledColor: AppColors.btnDisabled,
              color: AppColors.red,
              onPressed: pageQtd != 0 && selectedPage > 1
                  ? () {
                      if (onPrevious != null) {
                        onPrevious!();
                      }
                    }
                  : null,
              icon: const Icon(
                Icons.arrow_left_outlined,
              )),
          SizedBox(
            width: Insets.paginantionNextPreviusButtonSpace,
          ),
          ...generateButtons(),
          SizedBox(
            width: Insets.paginantionNextPreviusButtonSpace,
          ),
          IconButton(
              iconSize: Insets.paginantionNextPreviusButtonSize,
              disabledColor: AppColors.btnDisabled,
              color: AppColors.red,
              onPressed: pageQtd != 0 && selectedPage != pageQtd
                  ? () {
                      if (onNext != null) {
                        onNext!();
                      }
                    }
                  : null,
              icon: const Icon(
                Icons.arrow_right_outlined,
              )),
        ]),
      );
    });
  }
}
