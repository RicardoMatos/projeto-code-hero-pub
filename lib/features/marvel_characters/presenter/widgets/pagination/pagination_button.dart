import 'package:flutter/material.dart';

import '../../../../../core/presenter/styles.dart';

class PaginationButton extends StatelessWidget {
  final bool selected;
  final int page;
  final bool applySpace;
  final Function(int page)? onTap;

  const PaginationButton(
      {super.key,
      this.selected = false,
      required this.page,
      this.onTap,
      this.applySpace = false});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) onTap!(page);
      },
      child: Container(
          key: const Key('pagination-button'),
          margin: EdgeInsets.only(
              right: applySpace ? Insets.paginationButtonSpace : 0),
          decoration: BoxDecoration(
              color: selected ? AppColors.red : Colors.white,
              border: Border.all(color: AppColors.red),
              borderRadius: BorderRadius.circular(50)),
          height: Insets.paginationButtonSize,
          width: Insets.paginationButtonSize,
          child: Center(
            child: Text(
              "$page",
              style: AppTextStyle.paginateButton
                  .copyWith(color: selected ? Colors.white : AppColors.red),
            ),
          )),
    );
  }
}
