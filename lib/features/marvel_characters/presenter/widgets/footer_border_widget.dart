import 'package:flutter/material.dart';

import '../../../../core/presenter/styles.dart';

class FooterBorder extends StatelessWidget {
  const FooterBorder({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: Container(
            height: Insets.footerBorderHeight,
            color: AppColors.red,
          ),
        )
      ],
    );
  }
}
