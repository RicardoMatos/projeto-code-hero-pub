import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../../core/di/di.dart';
import '../../../../../core/domain/service/presenter_specs_service.dart';
import '../../../../../core/presenter/styles.dart';
import '../labels/app_bar_title_widget.dart';
import '../labels/candidate_name_widget.dart';

class AppBarWidget extends StatelessWidget {
  const AppBarWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return Container(
            margin: EdgeInsets.only(
                top: Insets.marginTop,
                left: Insets.marginHorizontalFixed,
                right: Insets.marginHorizontalFixed),
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    if (Navigator.canPop(context))
                      IconButton(
                        iconSize: 28,
                        color: AppColors.red,
                        icon: const Icon(Icons.keyboard_arrow_left_outlined),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    SizedBox(
                      width: Insets.listHeaderSpace,
                    ),
                    Column(
                      children: const [
                        AppBarTitle(),
                      ],
                    ),
                  ],
                ),
                if (DI.get<PresenterSpecsService>().deviceWidth >
                    FormFactor.desktop)
                  const CandidateName(
                    name: "Ricardo Matos",
                  )
              ],
            ));
      },
    );
  }
}
