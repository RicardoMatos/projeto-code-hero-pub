import 'package:flutter/material.dart';

import '../../../../../core/presenter/styles.dart';

class FilterCharactersInput extends StatelessWidget {
  final TextEditingController textEditingController;
  FilterCharactersInput({super.key, required this.textEditingController});

  final border = OutlineInputBorder(
      borderSide: BorderSide(
    color: AppColors.inputBorderColor,
  ));

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            margin:
                EdgeInsets.symmetric(horizontal: Insets.marginHorizontalFixed),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Nome do Personagem",
                  style: AppTextStyle.inputLabel,
                ),
                Container(
                    constraints: const BoxConstraints(maxWidth: 400),
                    height: 31,
                    child: TextField(
                      controller: textEditingController,
                      cursorColor: AppColors.red,
                      decoration: InputDecoration(
                          enabledBorder: border, focusedBorder: border),
                    ))
              ],
            ),
          ),
        ),
      ],
    );
  }
}
