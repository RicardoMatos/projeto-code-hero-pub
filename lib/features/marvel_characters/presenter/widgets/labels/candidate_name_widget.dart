import 'package:flutter/material.dart';

import '../../../../../core/presenter/styles.dart';

class CandidateName extends StatelessWidget {
  final String name;
  const CandidateName({required this.name, super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      name.toUpperCase(),
      style: AppTextStyle.titleHeaderLight,
    );
  }
}
