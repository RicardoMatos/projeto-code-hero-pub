import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../../core/presenter/styles.dart';

class AppBarTitle extends StatelessWidget {
  const AppBarTitle({super.key});

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Observer(builder: (_) {
            return RichText(
                text: TextSpan(
              style: AppTextStyle.titleHeaderBlack
                  .copyWith(fontSize: AppFontSize.headerTitle),
              children: [
                const TextSpan(text: "BUSCA MARVEL "),
                TextSpan(
                    text: "TESTE FRONT-END",
                    style: AppTextStyle.titleHeaderLight
                        .copyWith(fontSize: AppFontSize.headerTitle)),
              ],
            ));
          }),
          Container(
            color: AppColors.red,
            height: 4,
            width: 54,
          ),
        ],
      ),
    );
  }
}
