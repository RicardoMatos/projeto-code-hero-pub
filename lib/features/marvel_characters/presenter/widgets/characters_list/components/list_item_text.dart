import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../../../core/presenter/styles.dart';

class ListItemText extends StatelessWidget {
  final List<String> texts;
  const ListItemText({super.key, required this.texts});

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return Container(
        padding: EdgeInsets.all(Insets.paddingContentCell),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: texts
              .map((e) => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        e,
                        overflow: TextOverflow.ellipsis,
                        style: AppTextStyle.listCellsText,
                      )
                    ],
                  ))
              .toList(),
        ),
      );
    });
  }
}
