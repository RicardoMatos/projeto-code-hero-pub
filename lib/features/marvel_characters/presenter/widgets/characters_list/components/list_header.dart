import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../../../core/di/di.dart';
import '../../../../../../core/domain/service/presenter_specs_service.dart';
import '../../../../../../core/presenter/styles.dart';
import 'list_column_header.dart';

class ListHeader extends StatelessWidget {
  const ListHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        if (DI.get<PresenterSpecsService>().deviceWidth > FormFactor.desktop) {
          return const _Large();
        }

        return const _Narrow();
      },
    );
  }
}

class _Narrow extends StatelessWidget {
  const _Narrow();

  @override
  Widget build(BuildContext context) {
    return const ListColumnHeader(text: "Nome");
  }
}

class _Large extends StatelessWidget {
  const _Large();

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      const Expanded(flex: 2, child: ListColumnHeader(text: "Personagem")),
      SizedBox(
        width: Insets.listHeaderSpace,
      ),
      const Expanded(flex: 2, child: ListColumnHeader(text: "Séries")),
      SizedBox(
        width: Insets.listHeaderSpace,
      ),
      const Expanded(flex: 3, child: ListColumnHeader(text: "Eventos"))
    ]);
  }
}
