import 'package:flutter/material.dart';

import '../../../../../../core/presenter/styles.dart';

class ListColumnHeader extends StatelessWidget {
  final String text;
  const ListColumnHeader({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: Insets.listHeaderSpace),
        color: AppColors.red,
        height: Insets.listHeaderHeight,
        child: Row(
          children: [
            Text(
              text,
              overflow: TextOverflow.ellipsis,
              style: AppTextStyle.listHeaderLabel,
            ),
          ],
        ));
  }
}
