import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../../../core/presenter/styles.dart';

class ListItemWithAvatar extends StatelessWidget {
  final String img;
  final String text;

  const ListItemWithAvatar({super.key, required this.img, required this.text});

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return Container(
          padding: EdgeInsets.all(Insets.paddingContentCell),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ClipOval(
                child: Image.network(
                  img,
                  height: Insets.avatarSize,
                  width: Insets.avatarSize,
                ),
              ),
              SizedBox(
                width: Insets.spaceBetweenAvatarText,
              ),
              Flexible(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      text,
                      style: AppTextStyle.listCellsText,
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
