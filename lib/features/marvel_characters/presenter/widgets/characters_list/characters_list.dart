import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../../core/presenter/styles.dart';
import '../../../domain/entities/character_entity.dart';
import 'components/list_header.dart';
import 'list_row.dart';

class CharactersList extends StatelessWidget {
  final List<CharacterEntity> characters;
  const CharactersList({super.key, this.characters = const []});

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return Expanded(
        child: Container(
            margin: EdgeInsets.symmetric(horizontal: Insets.marginHorizontal),
            child: CustomScrollView(
              slivers: [
                SliverAppBar(
                  primary: false,
                  pinned: true,
                  automaticallyImplyLeading: false,
                  titleSpacing: 0,
                  collapsedHeight: Insets.listHeaderHeight,
                  toolbarHeight: Insets.listHeaderHeight,
                  elevation: 0,
                  backgroundColor: Colors.white,
                  title: const ListHeader(),
                ),
                SliverFixedExtentList(
                    itemExtent: Insets.listRowHeight,
                    delegate: SliverChildListDelegate.fixed(
                        [...characters.map((e) => ListRow(entity: e))]))
              ],
            )),
      );
    });
  }
}
