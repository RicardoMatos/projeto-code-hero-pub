import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../../core/di/di.dart';
import '../../../../../core/domain/service/presenter_specs_service.dart';
import '../../../../../core/presenter/styles.dart';
import '../../../../../core/utils/app_routes.dart';
import '../../../domain/entities/character_entity.dart';
import 'components/list_item_text.dart';
import 'components/list_item_with_avatar.dart';

class ListRow extends StatefulWidget {
  final CharacterEntity entity;

  const ListRow({super.key, required this.entity});

  @override
  State<ListRow> createState() => _ListRowState();
}

class _ListRowState extends State<ListRow> {
  bool isHover = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (event) => setState(() {
        isHover = true;
      }),
      onExit: (event) {
        setState(() {
          isHover = false;
        });
      },
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, AppRoutes.detail,
              arguments: widget.entity);
        },
        child: Container(
          color: isHover ? AppColors.rowHoverColor : Colors.transparent,
          height: Insets.listRowHeight,
          child: Observer(
            builder: (context) {
              if (DI.get<PresenterSpecsService>().deviceWidth >
                  FormFactor.desktop) {
                return Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: ListItemWithAvatar(
                            img: widget.entity.image.standardMedium,
                            text: widget.entity.name)),
                    Expanded(
                        flex: 2,
                        child: ListItemText(
                            texts: widget.entity.series.items
                                .take(3)
                                .map((e) => e.name)
                                .toList())),
                    Expanded(
                        flex: 3,
                        child: ListItemText(
                            texts: widget.entity.events.items
                                .take(3)
                                .map((e) => e.name)
                                .toList())),
                  ],
                );
              }
              return ListItemWithAvatar(
                  img: widget.entity.image.standardMedium,
                  text: widget.entity.name);
            },
          ),
        ),
      ),
    );
  }
}
