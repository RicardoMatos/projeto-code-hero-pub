import 'package:flutter/material.dart';

import '../../domain/entities/character_entity.dart';
import '../widgets/app_bar/app_bar_widget.dart';
import '../widgets/summary/character_summary_widget.dart';

class DetailPage extends StatefulWidget {
  final CharacterEntity character;

  const DetailPage({super.key, required this.character});

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const AppBarWidget(),
            CharacterSummary(character: widget.character)
          ],
        ),
      ),
    );
  }
}
