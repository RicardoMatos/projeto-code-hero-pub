import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../../core/di/di.dart';
import '../../../../core/domain/service/presenter_specs_service.dart';
import '../../../../core/presenter/styles.dart';
import '../controllers/home_page_controller.dart';
import '../widgets/app_bar/app_bar_widget.dart';
import '../widgets/characters_list/characters_list.dart';
import '../widgets/footer_border_widget.dart';
import '../widgets/inputs/filter_characters_input.dart';
import '../widgets/pagination/pagination_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _pageController = DI.get<HomePageController>();

  @override
  void initState() {
    _pageController.loadCharacters(1);
    super.initState();
  }

  _onNext() {
    final nextPage = _pageController.currentPage + 1;
    if (_pageController.filtering) {
      _pageController.loadCharactersByFiltering(nextPage);
    } else {
      _pageController.loadCharacters(nextPage);
    }
  }

  _onPrevious() {
    final previousPage = _pageController.currentPage - 1;
    if (_pageController.filtering) {
      _pageController.loadCharactersByFiltering(previousPage);
    } else {
      _pageController.loadCharacters(previousPage);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Observer(builder: (_) {
      return Column(
        children: [
          const AppBarWidget(),
          SizedBox(
            height: Insets.verticalElementsSpace,
          ),
          FilterCharactersInput(
              textEditingController: _pageController.textEditingController),
          SizedBox(
            height: Insets.verticalElementsSpace,
          ),
          if (_pageController.loading)
            const Expanded(
              child: Center(
                child: CircularProgressIndicator(
                  color: AppColors.red,
                  strokeWidth: 2,
                ),
              ),
            )
          else if (_pageController.characters.isEmpty)
            const Expanded(
                child: Center(
              child: Text("Não há items para sua busca"),
            ))
          else
            CharactersList(
              characters: _pageController.characters,
            ),
          PaginationWidget(
            maxButtons:
                DI.get<PresenterSpecsService>().deviceWidth > FormFactor.tablet
                    ? 6
                    : 3,
            pageQtd: _pageController.pagesQtd,
            selectedPage: _pageController.currentPage,
            onSelectPage: (page) => _pageController.loading
                ? null
                : _pageController.loadCharacters(page),
            onNext: () => _pageController.loading ? null : _onNext(),
            onPrevious: () => _pageController.loading ? null : _onPrevious(),
          ),
          const FooterBorder()
        ],
      );
    }));
  }
}
