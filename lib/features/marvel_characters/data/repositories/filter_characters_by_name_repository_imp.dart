import 'package:dartz/dartz.dart';

import '../../../../core/erros/failure.dart';
import '../../../../core/data/repositories/model/reponse_model.dart';
import '../../domain/repositories/filter_characters_by_name_repository.dart';
import '../datasources/filter_characters_by_name_datasource.dart';

class FilterCharactersByNameRepositoryImp
    implements FilterCharactersByNameRepository {
  final FilterCharactersByNameDataSource _dataSource;

  FilterCharactersByNameRepositoryImp(this._dataSource);

  @override
  Future<Either<Failure, ResponseDataContainer>> call(
      {required int offset, required int limit, required String name}) async {
    return (await _dataSource(limit: limit, name: name, offset: offset))
        .fold((l) => Left(l), (r) => Right(r.container));
  }
}
