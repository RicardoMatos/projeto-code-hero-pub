import 'package:dartz/dartz.dart';

import '../../../../core/data/repositories/model/reponse_model.dart';
import '../../../../core/erros/failure.dart';
import '../../domain/repositories/get_characters_repository.dart';
import '../datasources/get_characters_datasource.dart';

class GetCharactersRepositoryImp implements GetCharactersRepository {
  final GetCharactersDataSource _dataSource;

  GetCharactersRepositoryImp(this._dataSource);

  @override
  Future<Either<Failure, ResponseDataContainer>> call(
      {required int offset, required int limit}) async {
    return (await _dataSource(offset: offset, limit: limit))
        .fold((l) => Left(l), (r) => Right(r.container));
  }
}
