import '../../domain/entities/story_entity.dart';
import 'image_dto.dart';

class StoryDTO extends StoreEntity {
  const StoryDTO(
      {required super.id,
      required super.title,
      required super.description,
      required super.resourceUri,
      required super.thumbnail});

  factory StoryDTO.fromJson(Map<String, dynamic> json) => StoryDTO(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      resourceUri: json['resourceURI'],
      thumbnail: ImageDTO.fromJson(json['thumbnail']));
}
