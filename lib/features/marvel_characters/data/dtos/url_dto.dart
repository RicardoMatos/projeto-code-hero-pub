import '../../domain/entities/url_entity.dart';

class UrlDTO extends UrlEntity {
  const UrlDTO({required super.type, required super.url});

  factory UrlDTO.fromJson(Map<String, dynamic> json) =>
      UrlDTO(type: json['type'], url: json['url']);
}
