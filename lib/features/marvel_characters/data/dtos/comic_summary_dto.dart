import '../../domain/entities/comic_summary_entity.dart';

class ComicSummaryDTO extends ComicSummaryEntity {
  const ComicSummaryDTO({required super.resourceUri, required super.name});

  factory ComicSummaryDTO.fromJson(Map<String, dynamic> json) =>
      ComicSummaryDTO(resourceUri: json['resourceURI'], name: json['name']);
}
