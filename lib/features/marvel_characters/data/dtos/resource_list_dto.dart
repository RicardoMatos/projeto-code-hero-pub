import '../../domain/entities/resource_list.dart';

class ResourceListDTO<T> extends ResourceList<T> {
  const ResourceListDTO({
    required super.available,
    required super.returned,
    required super.items,
    required super.collectionUri,
  });

  factory ResourceListDTO.fromJson(Map<String, dynamic> json,
          T Function(Map<String, dynamic> json)? fromJsonItem) =>
      ResourceListDTO(
        available: json['available'],
        returned: json['returned'],
        items: (json['items'] as List)
            .map(
              (e) => fromJsonItem!(e),
            )
            .toList(),
        collectionUri: json['collectionURI'],
      );
}
