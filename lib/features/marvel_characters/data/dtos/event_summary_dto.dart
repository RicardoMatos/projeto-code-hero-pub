import '../../domain/entities/event_summary_entity.dart';

class EventSummaryDTO extends EventSummaryEntity {
  const EventSummaryDTO({required super.resourceUri, required super.name});

  factory EventSummaryDTO.fromJson(Map<String, dynamic> json) =>
      EventSummaryDTO(resourceUri: json['resourceURI'], name: json['name']);
}
