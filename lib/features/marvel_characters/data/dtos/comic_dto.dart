import '../../domain/entities/comic_entity.dart';
import 'image_dto.dart';

class ComicDTO extends ComicEntity {
  const ComicDTO(
      {required super.id,
      required super.title,
      required super.description,
      required super.thumbnail,
      required super.images});

  factory ComicDTO.fromJson(Map<String, dynamic> json) {
    return ComicDTO(
        id: json['id'],
        title: json['title'],
        description: json['description'],
        thumbnail: ImageDTO.fromJson(json['thumbnail']),
        images:
            (json['images'] as List).map((e) => ImageDTO.fromJson(e)).toList());
  }
}
