import '../../domain/entities/event_entity.dart';

import 'image_dto.dart';

class EventDTO extends EventEntity {
  const EventDTO(
      {required super.id,
      required super.title,
      required super.description,
      required super.thumbnail});

  factory EventDTO.fromJson(Map<String, dynamic> json) => EventDTO(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      thumbnail: ImageDTO.fromJson(json['thumbnail']));
}
