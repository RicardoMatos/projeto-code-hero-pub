import '../../domain/entities/image_entity.dart';

class ImageDTO extends ImageEntity {
  const ImageDTO({required super.path, required super.extension});

  factory ImageDTO.fromJson(Map<String, dynamic> json) =>
      ImageDTO(path: json['path'], extension: json['extension']);
}
