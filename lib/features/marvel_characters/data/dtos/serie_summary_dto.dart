import '../../domain/entities/serie_summary_entity.dart';

class SerieSummaryDTO extends SerieSummaryEntity {
  const SerieSummaryDTO({required super.resourceUri, required super.name});

  factory SerieSummaryDTO.fromJson(Map<String, dynamic> json) =>
      SerieSummaryDTO(resourceUri: json['resourceURI'], name: json['name']);
}
