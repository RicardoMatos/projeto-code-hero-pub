import '../../domain/entities/story_summary_entity.dart';

class StorySummaryDTO extends StorySummaryEntity {
  const StorySummaryDTO(
      {required super.resourceUri, required super.name, required super.type});

  factory StorySummaryDTO.fromJson(Map<String, dynamic> json) =>
      StorySummaryDTO(
          resourceUri: json['resourceURI'],
          name: json['name'],
          type: json['type']);
}
