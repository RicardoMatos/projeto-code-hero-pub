import '../../domain/entities/character_entity.dart';
import 'comic_summary_dto.dart';
import 'event_summary_dto.dart';
import 'image_dto.dart';
import 'resource_list_dto.dart';
import 'serie_summary_dto.dart';
import 'story_summary_dto.dart';
import 'url_dto.dart';

class CharacterDTO extends CharacterEntity {
  const CharacterDTO(
      {required super.id,
      required super.name,
      required super.description,
      required super.modified,
      required super.resourceURI,
      required super.urls,
      required super.image,
      required super.comics,
      required super.events,
      required super.stories,
      required super.series});

  factory CharacterDTO.fromJson(Map<String, dynamic> json) {
    return CharacterDTO(
        id: json['id'],
        name: json['name'],
        description: json['description'],
        modified: DateTime.tryParse(json['modified']) ?? DateTime.now(),
        resourceURI: json['resourceURI'],
        urls: (json['urls'] as List).map((e) => UrlDTO.fromJson(e)).toList(),
        image: ImageDTO.fromJson(json['thumbnail']),
        events: ResourceListDTO.fromJson(
            json['events'], (json) => EventSummaryDTO.fromJson(json)),
        comics: ResourceListDTO.fromJson(
            json['comics'], (json) => ComicSummaryDTO.fromJson(json)),
        stories: ResourceListDTO.fromJson(
            json['stories'], (json) => StorySummaryDTO.fromJson(json)),
        series: ResourceListDTO.fromJson(
            json['series'], (json) => SerieSummaryDTO.fromJson(json)));
  }
}
