import 'package:dartz/dartz.dart';

import '../../../../core/data/repositories/model/reponse_model.dart';
import '../../../../core/erros/failure.dart';

abstract class GetCharactersDataSource {
  Future<Either<Failure, ResponseDataWrapper>> call(
      {required int offset, required int limit});
}
