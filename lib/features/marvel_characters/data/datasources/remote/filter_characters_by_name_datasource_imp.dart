import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../../../../../core/data/repositories/model/reponse_model.dart';
import '../../../../../core/domain/service/http_service.dart';
import '../../../../../core/erros/failure.dart';
import '../filter_characters_by_name_datasource.dart';

class FilterCharactersByNameDataSourceImp
    implements FilterCharactersByNameDataSource {
  final HttpService _httpService;

  FilterCharactersByNameDataSourceImp(this._httpService);

  @override
  Future<Either<Failure, ResponseDataWrapper>> call(
      {required int offset, required int limit, required String name}) async {
    try {
      final response = await _httpService.get('/characters',
          params: {'offset': offset, 'limit': limit, 'name': name});
      return Right(ResponseDataWrapper.fromMap(response.data));
    } on DioError {
      return Left(RequestFailure());
    } on Exception {
      return Left(ProcessRequestFailure());
    }
  }
}
