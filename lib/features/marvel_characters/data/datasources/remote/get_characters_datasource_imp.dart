import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../../../../../core/data/repositories/model/reponse_model.dart';
import '../../../../../core/domain/service/http_service.dart';
import '../../../../../core/erros/failure.dart';
import '../get_characters_datasource.dart';

class GetCharactersDataSourceImp implements GetCharactersDataSource {
  final HttpService _httpService;

  GetCharactersDataSourceImp(this._httpService);

  @override
  Future<Either<Failure, ResponseDataWrapper>> call(
      {required int offset, required int limit}) async {
    try {
      final response = await _httpService
          .get('/characters', params: {'offset': offset, 'limit': limit});
      return Right(ResponseDataWrapper.fromMap(response.data));
    } on DioError {
      return Left(RequestFailure());
    } on Exception {
      return Left(ProcessRequestFailure());
    }
  }
}
