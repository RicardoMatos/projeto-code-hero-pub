import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'core/di/di.dart';
import 'core/domain/service/presenter_specs_service.dart';
import 'core/utils/app_routes.dart';
import 'features/marvel_characters/domain/entities/character_entity.dart';
import 'features/marvel_characters/presenter/pages/detail_page.dart';
import 'features/marvel_characters/presenter/pages/home_page.dart';

void main() async {
  DI.init();
  await dotenv.load();
  runApp(const RootApp());
}

class RootApp extends StatefulWidget {
  const RootApp({super.key});

  @override
  State<RootApp> createState() => _RootAppState();
}

class _RootAppState extends State<RootApp> {
  late PresenterSpecsService presenterSpecs;

  @override
  void initState() {
    presenterSpecs = DI.get<PresenterSpecsService>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        presenterSpecs.setDeviceWidth(constraints.maxWidth);
        return MaterialApp(
          initialRoute: AppRoutes.home,
          routes: {
            AppRoutes.home: (context) => const HomePage(),
            AppRoutes.detail: (context) => DetailPage(
                  character: ModalRoute.of(context)?.settings.arguments
                      as CharacterEntity,
                )
          },
        );
      },
    );
  }
}
