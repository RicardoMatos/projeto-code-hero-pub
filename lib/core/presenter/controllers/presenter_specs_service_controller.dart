import 'package:mobx/mobx.dart';

import '../../domain/service/presenter_specs_service.dart';
import '../styles.dart';

part 'presenter_specs_service_controller.g.dart';

class PresenterSpecsServiceController = _PresenterSpecsServiceControllerBase
    with _$PresenterSpecsServiceController;

abstract class _PresenterSpecsServiceControllerBase
    with Store
    implements PresenterSpecsService {
  @observable
  ScreenSize _screenSize = ScreenSize.small;

  @observable
  double _deviceWidth = 0;

  @override
  @computed
  ScreenSize get screenSize => _screenSize;

  @override
  double get deviceWidth => _deviceWidth;

  @override
  @action
  void setDeviceWidth(double width) {
    if (_deviceWidth == width) return;
    _deviceWidth = width;
    _screenSize = _getScrenSize(_deviceWidth);
  }

  ScreenSize _getScrenSize(double width) {
    if (width > FormFactor.desktop) return ScreenSize.extraLarge;
    if (width > FormFactor.tablet) return ScreenSize.large;
    if (width > FormFactor.handset) return ScreenSize.normal;
    return ScreenSize.small;
  }
}
