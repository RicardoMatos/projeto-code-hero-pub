// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'presenter_specs_service_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$PresenterSpecsServiceController
    on _PresenterSpecsServiceControllerBase, Store {
  Computed<ScreenSize>? _$screenSizeComputed;

  @override
  ScreenSize get screenSize =>
      (_$screenSizeComputed ??= Computed<ScreenSize>(() => super.screenSize,
              name: '_PresenterSpecsServiceControllerBase.screenSize'))
          .value;

  late final _$_screenSizeAtom = Atom(
      name: '_PresenterSpecsServiceControllerBase._screenSize',
      context: context);

  @override
  ScreenSize get _screenSize {
    _$_screenSizeAtom.reportRead();
    return super._screenSize;
  }

  @override
  set _screenSize(ScreenSize value) {
    _$_screenSizeAtom.reportWrite(value, super._screenSize, () {
      super._screenSize = value;
    });
  }

  late final _$_deviceWidthAtom = Atom(
      name: '_PresenterSpecsServiceControllerBase._deviceWidth',
      context: context);

  @override
  double get _deviceWidth {
    _$_deviceWidthAtom.reportRead();
    return super._deviceWidth;
  }

  @override
  set _deviceWidth(double value) {
    _$_deviceWidthAtom.reportWrite(value, super._deviceWidth, () {
      super._deviceWidth = value;
    });
  }

  late final _$_PresenterSpecsServiceControllerBaseActionController =
      ActionController(
          name: '_PresenterSpecsServiceControllerBase', context: context);

  @override
  void setDeviceWidth(double width) {
    final _$actionInfo =
        _$_PresenterSpecsServiceControllerBaseActionController.startAction(
            name: '_PresenterSpecsServiceControllerBase.setDeviceWidth');
    try {
      return super.setDeviceWidth(width);
    } finally {
      _$_PresenterSpecsServiceControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
screenSize: ${screenSize}
    ''';
  }
}
