import 'package:flutter/material.dart';

import '../di/di.dart';
import '../domain/service/presenter_specs_service.dart';

class FormFactor {
  static double desktop = 900;
  static double tablet = 600;
  static double handset = 300;
}

class Insets {
  static double listHeaderSpace = 10;
  static double listHeaderHeight = 30;
  static double listRowHeight = 112;
  static double marginHorizontalFixed = 42;
  static double avatarSize = 58;
  static double spaceBetweenAvatarText = 25;
  static double footerBorderHeight = 12;
  static double paginationButtonSize = 32;
  static double paginantionNextPreviusButtonSize = 29;
  static double paginantionTopMargin = 18;

  static double get paginantionBottomMargin {
    switch (DI.get<PresenterSpecsService>().screenSize) {
      case ScreenSize.small:
      case ScreenSize.normal:
        return 16;
      case ScreenSize.large:
      case ScreenSize.extraLarge:
        return 24;
    }
  }

  static double get paginantionNextPreviusButtonSpace {
    switch (DI.get<PresenterSpecsService>().screenSize) {
      case ScreenSize.small:
      case ScreenSize.normal:
        return 60;
      case ScreenSize.large:
      case ScreenSize.extraLarge:
        return 12;
    }
  }

  static double paginationButtonSpace = 20;

  static double get marginHorizontal {
    switch (DI.get<PresenterSpecsService>().screenSize) {
      case ScreenSize.small:
      case ScreenSize.normal:
      case ScreenSize.large:
        return 0;
      case ScreenSize.extraLarge:
        return 42;
    }
  }

  static double get marginTop {
    switch (DI.get<PresenterSpecsService>().screenSize) {
      case ScreenSize.small:
      case ScreenSize.normal:
        return 12;
      case ScreenSize.large:
      case ScreenSize.extraLarge:
        return 20;
    }
  }

  static double get paddingContentCell {
    switch (DI.get<PresenterSpecsService>().screenSize) {
      case ScreenSize.small:
      case ScreenSize.normal:
        return 18;
      case ScreenSize.large:
      case ScreenSize.extraLarge:
        return 20;
    }
  }

  static double get verticalElementsSpace {
    switch (DI.get<PresenterSpecsService>().screenSize) {
      case ScreenSize.small:
      case ScreenSize.normal:
        return 12;
      case ScreenSize.large:
      case ScreenSize.extraLarge:
        return 34;
    }
  }
}

class AppTextStyle {
  static final titleHeaderLight = TextStyle(
      fontFamily: 'RobotoLight',
      fontSize: AppFontSize.headerTitle,
      height: 1.2,
      color: AppColors.red);

  static final titleHeaderBlack = TextStyle(
      fontFamily: 'RobotoBlack',
      fontSize: AppFontSize.headerTitle,
      height: 1.2,
      color: AppColors.red);

  static final inputLabel = TextStyle(
      fontFamily: 'Roboto',
      height: 1.2,
      color: AppColors.red,
      fontSize: AppFontSize.inputLabel);

  static const listHeaderLabel = TextStyle(
      fontFamily: 'Roboto', fontSize: 16, height: 1.2, color: Colors.white);

  static const listCellsText = TextStyle(
      fontFamily: 'Roboto',
      fontSize: 21,
      height: 1.1,
      color: AppColors.textColor);

  static const paginateButton =
      TextStyle(fontFamily: 'Roboto', fontSize: 21, height: 1.1);
}

class AppFontSize {
  static const double font27 = 27;
  static double get headerTitle {
    switch (DI.get<PresenterSpecsService>().screenSize) {
      case ScreenSize.small:
      case ScreenSize.normal:
        return 16;
      case ScreenSize.large:
      case ScreenSize.extraLarge:
        return 27;
    }
  }

  static double get inputLabel {
    switch (DI.get<PresenterSpecsService>().screenSize) {
      case ScreenSize.small:
      case ScreenSize.normal:
        return 16;
      case ScreenSize.large:
      case ScreenSize.extraLarge:
        return 16;
    }
  }
}

class AppColors {
  static const Color red = Color(0xFFD42026);
  static Color btnDisabled = const Color(0xFFD20A0A).withOpacity(.35);
  static const Color textColor = Color(0xFF4E4E4E);
  static Color rowHoverColor = const Color(0xFFD42026).withOpacity(.1);
  static Color inputBorderColor = const Color(0xFFA5A5A5);
}
