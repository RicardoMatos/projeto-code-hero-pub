import 'dart:convert';

// ignore_for_file: public_member_api_docs, sort_constructors_first
class ResponseDataWrapper {
  final int code;
  final String status;
  final String copyright;
  final String attributionText;
  final String attributionHtml;
  final ResponseDataContainer container;

  ResponseDataWrapper({
    required this.code,
    required this.status,
    required this.copyright,
    required this.attributionText,
    required this.attributionHtml,
    required this.container,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'code': code,
      'status': status,
      'copyright': copyright,
      'attributionText': attributionText,
      'attributionHtml': attributionHtml,
      'container': container.toMap(),
    };
  }

  factory ResponseDataWrapper.fromMap(Map<String, dynamic> map) {
    return ResponseDataWrapper(
      code: map['code'] as int,
      status: map['status'] as String,
      copyright: map['copyright'] as String,
      attributionText: map['attributionText'] as String,
      attributionHtml: map['attributionHTML'] as String,
      container:
          ResponseDataContainer.fromMap(map['data'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory ResponseDataWrapper.fromJson(String source) =>
      ResponseDataWrapper.fromMap(json.decode(source) as Map<String, dynamic>);
}

class ResponseDataContainer {
  final int offset;
  final int limit;
  final int total;
  final int count;
  final List<Map<String, dynamic>> results;

  ResponseDataContainer({
    required this.offset,
    required this.limit,
    required this.total,
    required this.count,
    required this.results,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'offset': offset,
      'limit': limit,
      'total': total,
      'count': count,
      'results': results,
    };
  }

  factory ResponseDataContainer.fromMap(Map<String, dynamic> map) {
    return ResponseDataContainer(
      offset: map['offset'] as int,
      limit: map['limit'] as int,
      total: map['total'] as int,
      count: map['count'] as int,
      results: List<Map<String, dynamic>>.from(
        (map['results'] as List).map<Map<String, dynamic>>(
          (x) => x,
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory ResponseDataContainer.fromJson(String source) =>
      ResponseDataContainer.fromMap(
          json.decode(source) as Map<String, dynamic>);
}
