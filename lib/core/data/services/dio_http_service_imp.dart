import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../../domain/service/http_service.dart';

class DioHttpServiceImp implements HttpService {
  late Dio _dio;

  DioHttpServiceImp() {
    _dio = Dio(BaseOptions(
      baseUrl: "https://gateway.marvel.com/v1/public",
      headers: {'content-type': 'application/json;charset=utf-8'},
    ));

    _dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, handler) {
        options.queryParameters.addAll(_authenticateRequest());
        return handler.next(options);
      },
    ));
  }

  Map<String, dynamic> _authenticateRequest() {
    final params = <String, dynamic>{};
    final apiKey = dotenv.env['API_KEY'];
    params['apikey'] = apiKey;

    if (!kIsWeb) {
      final privateKey = dotenv.env['API_SECRET'];
      final timestamp = DateTime.now().toIso8601String();
      final hash =
          md5.convert(utf8.encode("$timestamp$privateKey$apiKey")).toString();

      params['ts'] = timestamp;
      params['hash'] = hash;
    }
    return params;
  }

  @override
  Future<Response<T>> get<T>(String path,
      {Map<String, dynamic>? params}) async {
    return _dio.get(path, queryParameters: params);
  }
}
