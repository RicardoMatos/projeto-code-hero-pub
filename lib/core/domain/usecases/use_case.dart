import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../erros/failure.dart';

/// Contract that every usecase must be implement
abstract class UseCase<Type, Params> {
  Future<Either<Failure, Type>> call(Params params);
}

/// Indicates a UseCase Empty Params
class NoParams extends Equatable {
  @override
  List<Object?> get props => [];
}
