enum ScreenSize { small, normal, large, extraLarge }

abstract class PresenterSpecsService {
  void setDeviceWidth(double width);

  ScreenSize get screenSize;
  double get deviceWidth;
}
