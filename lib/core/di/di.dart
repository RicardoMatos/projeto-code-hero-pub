import 'package:get_it/get_it.dart';

import '../../features/marvel_characters/data/datasources/filter_characters_by_name_datasource.dart';
import '../../features/marvel_characters/data/datasources/get_characters_datasource.dart';
import '../../features/marvel_characters/data/datasources/remote/filter_characters_by_name_datasource_imp.dart';
import '../../features/marvel_characters/data/datasources/remote/get_characters_datasource_imp.dart';
import '../../features/marvel_characters/data/repositories/filter_characters_by_name_repository_imp.dart';
import '../../features/marvel_characters/data/repositories/get_characters_repository_imp.dart';
import '../../features/marvel_characters/domain/repositories/filter_characters_by_name_repository.dart';
import '../../features/marvel_characters/domain/repositories/get_characters_repository.dart';
import '../../features/marvel_characters/domain/usecases/filter_characters_by_name_usecase/filter_characters_by_name_usecase.dart';
import '../../features/marvel_characters/domain/usecases/filter_characters_by_name_usecase/filter_characters_by_name_usecase_imp.dart';
import '../../features/marvel_characters/domain/usecases/get_characters_usercase/get_characters_usecase.dart';
import '../../features/marvel_characters/domain/usecases/get_characters_usercase/get_characters_usecase_imp.dart';
import '../../features/marvel_characters/presenter/controllers/characters_controller.dart';
import '../../features/marvel_characters/presenter/controllers/home_page_controller.dart';
import '../data/services/dio_http_service_imp.dart';
import '../domain/service/http_service.dart';
import '../domain/service/presenter_specs_service.dart';
import '../presenter/controllers/presenter_specs_service_controller.dart';

/// Dependencies Injections class
class DI {
  static T get<T extends Object>() {
    return GetIt.I.get<T>();
  }

  static init() {
    GetIt injector = GetIt.I;
    /**
     * core
     */
    injector.registerLazySingleton<HttpService>(() => DioHttpServiceImp());

    /**
     * datasources
     */
    injector.registerLazySingleton<GetCharactersDataSource>(
        () => GetCharactersDataSourceImp(injector()));
    injector.registerLazySingleton<FilterCharactersByNameDataSource>(
        () => FilterCharactersByNameDataSourceImp(injector()));

    /**
     * repositories
     */
    injector.registerLazySingleton<GetCharactersRepository>(
        () => GetCharactersRepositoryImp(injector()));
    injector.registerLazySingleton<FilterCharactersByNameRepository>(
        () => FilterCharactersByNameRepositoryImp(injector()));

    /**
     * usecases
     */
    injector.registerLazySingleton<GetCharactersUseCase>(
        () => GetCharactersUseCaseImp(injector()));
    injector.registerLazySingleton<FilterCharactersByNameUseCase>(
        () => FilterCharactersByNameUseCaseImp(injector()));

    /**
     * controllers
     */
    injector.registerLazySingleton<PresenterSpecsService>(
        () => PresenterSpecsServiceController());
    injector.registerLazySingleton(
        () => CharactersController(injector(), injector()));

    /**
     * Page Controllers
     */
    injector.registerLazySingleton(() => HomePageController(injector()));
  }
}
