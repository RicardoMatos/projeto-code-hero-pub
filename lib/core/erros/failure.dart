import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  final dynamic properties;

  const Failure([this.properties = const <dynamic>[]]);

  @override
  List<Object> get props => [properties];
}

class RequestFailure extends Failure {}

class ProcessRequestFailure extends Failure {}
