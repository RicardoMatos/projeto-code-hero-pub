# code_hero_project

Versão Flutter:

```
Flutter 3.7.4 • channel stable • https://github.com/flutter/flutter.git
Framework • revision b4bce91dd0 (4 weeks ago) • 2023-02-21 09:50:50 +0800
Engine • revision 248290d6d5
Tools • Dart 2.19.2 • DevTools 2.20.1
```

## Estrutura do Projeto

```
|--lib/
    |-- core - Define contratos e recursos globais utilizados por n features

    |-- features
        |-- feature_1
            |-- data/ camada de dados
                |-- datasources/ - Recursos externos
                |-- dto/ - Data Transfer Objects das respectivas entidades
                |-- repositories/ - Recursos responsavel por fazer chamadas externas
            |-- domain/ - camada de domínio
                |-- entities/ - Entidades & regras de negócio
                |-- repositories/ - Contratos para camada de data
                |-- usecases/ - Casos de uso da funcionalidade
            |-- presenter/ - Camada de View/Front
                |-- controllers/ - Controllers da feat
                |-- pages/ - Paginas da feat
                |-- widgets/ - componentes reutilizáveis para a feat


```
